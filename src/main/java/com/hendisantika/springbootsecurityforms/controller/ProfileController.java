package com.hendisantika.springbootsecurityforms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-forms
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/03/20
 * Time: 07.47
 */
@Controller
@RequestMapping("profile")
public class ProfileController {

    @GetMapping("index")
    public String index() {
        return "profile/index";
    }
}