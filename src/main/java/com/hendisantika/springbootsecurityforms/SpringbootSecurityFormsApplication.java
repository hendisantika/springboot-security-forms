package com.hendisantika.springbootsecurityforms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSecurityFormsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecurityFormsApplication.class, args);
    }

}
