# Spring Boot Security Form
## Things to do
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-security-forms.git`
2. Go to folder: `cd springboot-security-forms`
3. Run the application: `mvn clean spring-boot:run`
4. Use following user list

| # | Username | Password |
| --- | ---| ---|
| 1 | user | user123 |
| 2 | admin | admin123 |
| 3 | manager | manager123 |

## Screen shot

![Login Page](img/login.png "Login Page")

![Home Page](img/home.png "Home Page")

![Index Page](img/index.png "Index Page")

![Profile Page](img/profile.png "Profile Page")

![Admin Page](img/admin.png "Admin Page")

![User Page](img/user.png "User Page")

